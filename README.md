**Description du problématique:**
    Jean a un restaurant auquel il vend des sanswichs vietnamiennes. 
    Il découvre que de plus en plus, plusieurs restaurants commencent à 
    implémenter de la technologie dans leur environnement. Donc, il décide de
    vouloir créer une application auquel les clients peuvent faire leurs
    commandes en ligne ou à partir de leur téléphones mobiles. Il voudrait
    garder cela assez simple, donc il n'y aura pas de services de livraison 
    tout de suite. Donc, le client est obligé de faire les paiements dans le 
    restaurant. Juste que sa commande serait préparé en avance.

**Identification des acteurs:**
* La sandwicherie
* Les clients qui souhaitent acheter un/des sandwich(s).

**Description des cas d'utilisation (client):**
* Ajouter une commande;
* Modifier une commande;
* Supprimer une commande;
* Afficher la commande;
* Soumettre une commande;
     
**Description du projet de développement:**
Le cadre du projet consiste à créer un microservice pour la gestion et l'affichage du menu de commande. 
Le menu de l'application serait développer une API REST et une SOAP. 
Ensuite, Il faudrait créé un interface Android pour les téléphones 
mobiles (permettre aux clients de faire leurs commandes sur leur téléphone mobile).
